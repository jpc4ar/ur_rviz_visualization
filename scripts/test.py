#!/usr/bin/env python
import roslib
import rospy
import rosnode
from sensor_msgs.msg import JointState
from tf.transformations import euler_from_quaternion, quaternion_from_euler

##########################################
############### ROS ######################
##########################################

rospy.init_node('test', anonymous=False)
loop_rate = 30 # HZ
main_loop = rospy.Rate(loop_rate)
    
##########################################
############### Env ######################
##########################################
msg_joint = JointState()
msg_joint.header.frame_id = ''
msg_joint.header.stamp = rospy.Time.now()
#msg_joint.name = ['base', 'shoulder', 'elbow', 'wrist_1', 'wrist_2', 'wrist_3']
msg_joint.name = ["shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
msg_joint.effort = []
msg_joint.velocity = []
##########################################
############### Sub ######################
##########################################
pub_joints_position = rospy.Publisher('/joint_states',JointState,queue_size=1)

##########################################
########### Main loop ####################
##########################################

while not rospy.is_shutdown():
    
    # pub joints position
    msg_joint.position = [0,0.5,0,0,0,0]
    pub_joints_position.publish(msg_joint)
    main_loop.sleep()
